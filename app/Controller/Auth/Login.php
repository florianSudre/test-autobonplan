<?php



namespace app\Controller\Auth;

use app\Import\CSV\CSVConvertor;
use app\Model\Dealership;
use app\Model\DealershipCall;
use app\Model\User;
use Core\Controller\AbstractController;
use Core\Database\Database;
use Core\Tools\Converter\TimeConverter;
use DateTime;

class Login extends AbstractController
{

    public function index(){

        if($_SERVER['REQUEST_METHOD'] === "POST"){
            $email = $_REQUEST['email'];
            $Password = $_REQUEST['password'];

            $error = [];

            if(strlen($Password) < 8 )
            {
                $error['password'] = 'password length 8 minimum';
            }
            $regex = '/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$/';
            if (!preg_match($regex, $email)) {
                $error['email'] = "Invalid email format";
            }


            $user = User::queryS('SELECT * From user WHERE user.email = :email',['email' => $email])[0];
            if(sha1($Password) ===  $user->password){
                session_start();
                $_SESSION["email"] = [$email];
                $_SESSION["permission"] = [$user->permission];
                header("Location: http://local.test-auto.com/home");
                exit();
            }
            header("Location: http://local.test-auto.com/home");
            exit();
        }
        else
        {
            $this->render('Auth.login');
        }
    }


}


