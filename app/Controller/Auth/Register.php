<?php



namespace app\Controller\Auth;


use app\Model\User;
use Core\Controller\AbstractController;


class Register extends AbstractController
{

    public function index(){
        if($_SERVER['REQUEST_METHOD'] === "POST"){

            $name = htmlspecialchars($_REQUEST['name']);
            $email = $_REQUEST['email'];
            $Password = $_REQUEST['password'];


            $error = [];

            if(strlen($name) < 8 )
            {
                $error['name'] = 'name length 8 minimum';
            }
            if(strlen($Password) < 8 )
            {
                $error['password'] = 'password length 8 minimum';
            }
            $regex = '/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$/';
            if (!preg_match($regex, $email)) {
                $error['email'] = "Invalid email format";
            }


            $user = new User();
            $user->setName($name);
            $user->setEmail($email);
            $user->setPassword(sha1($Password));///sha1 is just for exercice use other hash
            $user->setPermission('[\"read\"]');
            $user->setCreatedAt(date('Y-m-d'));
            $result =$user->insert();


            session_start();
            $_SESSION["Redirect_Param"] = ['error'=>$error];
            header("Location: http://local.test-auto.com/home");

            exit();
        }
        else
        {
            $this->render('Auth.register');
        }
    }


}


