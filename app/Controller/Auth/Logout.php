<?php


namespace app\Controller\Auth;


use Core\Controller\AbstractController;


class Logout extends AbstractController
{
    public function index()
    {
        session_start();
        session_destroy();
        header("Location: http://local.test-auto.com/home");
        exit();
    }
}


