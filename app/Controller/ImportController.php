<?php


namespace app\Controller;




use app\Import\CSV\CSVConvertor;
use app\Model\Dealership;
use app\Model\DealershipCall;
use Core\Database\Database;
use Core\Tools\Converter\TimeConverter;
use DateTime;

class ImportController extends Controller
{

    public function import(){

        $uploaddir = $_SERVER['DOCUMENT_ROOT'];
        $uploadfile = $uploaddir . DIRECTORY_SEPARATOR.($_FILES['file']['name']);
        $mimetype = mime_content_type($_FILES['file']['tmp_name']);

        if ( $_FILES['file']['type'] === "text/csv" && move_uploaded_file($_FILES['file']['tmp_name'], $uploadfile)) {
            echo json_encode('le fichier '. $_FILES['file']['name'] .' a bien ete enregistre');
        } else {
            echo json_encode('fichier non importe');
        }


        ///LECTURE CSV STOCKE

        CSVConvertor::ConvertFromFile($_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR.'call.csv');

        $resultCsv = CSVConvertor::ConvertFromFile($_SERVER['DOCUMENT_ROOT'] .DIRECTORY_SEPARATOR.$_FILES['file']['name']);

        $dealerShipList =[];
        foreach($resultCsv as $line)
        {
            if(!key_exists($line["salepoint_phone"],$dealerShipList)){
                $dealerShipList[$line["salepoint_phone"]] = $line["salepoint"];
            }
        }

        $dealerShipArray=[];
        foreach($dealerShipList as $phone=>$name )
        {
            $dealerShip =new Dealership();
            $dealerShip->setName(htmlspecialchars($name));
            $dealerShip->setSalepointPhone($phone);
            $dealerShipArray[] = $dealerShip;
        }
        Database::getInstance()->insertMany($dealerShipArray,Dealership::class);

        $result = Database::getInstance()->query('SELECT ds.salepoint_phone,ds.id  FROM dealership as ds');
        $dealerShipIdList = [];

        foreach ($result as $item){
            $dealerShipIdList[$item->salepoint_phone] = $item->id;

        }
        $dealerShipCallArray=[];
        foreach($resultCsv as $line )
        {
            $dealerShipCall =new DealershipCall();
            $dealerShipCall->setExtId(htmlspecialchars($line["call_3cx_id"]));
            $date=DateTime::createFromFormat('d/m/Y H:i', $line["date"]);
            $dealerShipCall->setCallDate(date_format($date,"Y-m-d H:i:s"));
            $dealerShipCall->setDuration(TimeConverter::StringToSeconds($line["duration"]));
            $dealerShipCall->setDealershipId($dealerShipIdList[$line["salepoint_phone"]]);
            $dealerShipCallArray[] = $dealerShipCall;
        }
        $result = Database::getInstance()->insertMany($dealerShipCallArray,DealershipCall::class);
    }

}