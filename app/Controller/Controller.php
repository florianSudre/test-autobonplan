<?php

namespace app\Controller;

use Core\Controller\AbstractController;

abstract class Controller extends AbstractController{


    public function render($vue, $parameters = [])
    {
        if(!isset($_SESSION))
        {
            session_start();
        }
        if(isset($_SESSION['email']))
        {
            $user = ["email"=>$_SESSION['email'][0],"permission" => json_decode($_SESSION["permission"][0],true)] ;
            $permissionImport = array_search("import",$user['permission'])!==false;

            $parameters["sessionUser"] = $user;
            $parameters["permissionImport"] = $permissionImport;

            parent::render($vue, $parameters);

        }else{
            $user = false;
            $permissionImport = false;
            header("Location: http://local.test-auto.com/login");
        }



    }
}