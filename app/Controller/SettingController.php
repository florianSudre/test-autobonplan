<?php


namespace app\Controller;


use Core\Database\Database;

class SettingController extends Controller
{

    public function setting()
    {
        $dealerships = Database::getInstance()->query('SELECT * FROM dealership dc ORDER BY dc.name');
        $dealershipsFastSelection = [];

        foreach ($dealerships as $dealership ){
            if(!array_key_exists($dealership->name,$dealershipsFastSelection)){
                $dealershipsFastSelection[$dealership->name] = [];
            }
            $dealershipsFastSelection[$dealership->name][] = $dealership->id;
        }

        session_start();
        if ($_SERVER['REQUEST_METHOD'] === "POST") {
            $dealershipSelected = [];
            foreach ($_REQUEST as $key => $value) {
                if (str_contains($key, 'checked')) {
                    $dealershipSelected[] = explode('-', $key)[1];
                }
            }
            var_dump($_REQUEST["date"]);
            $_SESSION['dealership_selected'] = $dealershipSelected;
            $_SESSION['date_select'] = $_REQUEST["date"];
            $date = $_SESSION['date_select'];

            header("Location: http://local.test-auto.com/home");
        }
        foreach ($dealerships as $dealership) {

            if (isset($_SESSION['dealership_selected'])) {
                $dealership->checked = array_search($dealership->id, $_SESSION['dealership_selected']) !== false;
            } else {
                $dealership->checked = false;
            }
        }

        $this->render('setting', [
            "dealerships" => $dealerships,
            "date" => $_SESSION['date_select'] ?? date('Y-m-d'),
            "dealershipsFastSelection"=>$dealershipsFastSelection
        ]);
    }

}