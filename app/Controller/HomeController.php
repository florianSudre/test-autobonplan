<?php

namespace app\Controller;


use Core\Database\Database;
use Core\Tools\Converter\ArrayConverter;
use DateTime;

class HomeController extends Controller
{


    public function index()
    {
        session_start();


        if (isset($_SESSION["dealership_selected"]) && !empty($_SESSION["dealership_selected"]) && isset($_SESSION["date_select"])) {
            $dateTime = DateTime::createFromFormat('Y-m-d', $_SESSION["date_select"]);
            $date = date_format($dateTime, "Y-m-d");
            $day = date_format($dateTime, "D");
            switch ($day){
                case 'Mon' :$day ='lundi';break;
                case 'Tue' :$day ='mardi';break;
                case 'Wed' :$day ='mercredi';break;
                case 'Thu' :$day ='jeudi';break;
                case 'Fri' :$day ='vendredi';break;
                case 'Sat' :$day ='samedi';break;
                case 'Sun' :$day ='dimanche';break;
            }
            //Get Total call received and diff with previous week
            $arrayForQuery = ArrayConverter::arrayToStringSQL($_SESSION['dealership_selected']);

            $in = $arrayForQuery['in'];
            $paramQuery = $arrayForQuery['param'];

            $totalTakeInChargeByDealershipRequestResult = Database::getInstance()->query(
                "SELECT d.id ,d.name,COUNT(dc.id) as total,SUM(CASE WHEN dc.duration IS NULL THEN 0 ELSE 1 END ) AS take 
                        FROM dealership_call dc JOIN dealership d ON dc.dealership_id = d.id 
                        WHERE DAY(dc.call_date) = DAY(:dateref)
                        GROUP BY d.id  HAVING d.id IN ($in) ", array_merge(['dateref' => $date], $paramQuery));

            $totalTakeInChargeByDealershipRequestResultLastW = Database::getInstance()->query(
                "SELECT d.id ,d.name,COUNT(dc.id) as total, SUM(CASE WHEN dc.duration IS NULL THEN 0 ELSE 1 END ) AS take 
                        FROM dealership_call dc JOIN dealership d ON dc.dealership_id = d.id 
                        WHERE DAY(dc.call_date) = DAY(DATE_SUB(:dateref, INTERVAL 7 DAY)) 
                        GROUP BY d.id  HAVING d.id IN ($in) ", array_merge(['dateref' => $date], $paramQuery));



            $arrayForJs = [];
            $countCallThisDay = 0;
            $countCallTakeThisDay = 0;
            foreach ($totalTakeInChargeByDealershipRequestResult as $item) {
                if (!array_key_exists($item->name, $arrayForJs)) {
                    $arrayForJs[$item->name] = [];
                }
                if (!array_key_exists("Dn-0", $arrayForJs[$item->name])) {
                    $arrayForJs[$item->name]["Dn-0"] = 0;
                }
                if (!array_key_exists("Dn-0T", $arrayForJs[$item->name])) {
                    $arrayForJs[$item->name]["Dn-0T"] = 0;
                }
                $arrayForJs[$item->name]["Dn-0"] += $item->total;
                $arrayForJs[$item->name]["Dn-0T"] += $item->take;
                $countCallThisDay += $item->total;
                $countCallTakeThisDay += $item->take;
            }
            $countCallLastDay = 0;
            $countCallTakeLastDay = 0;
            foreach ($totalTakeInChargeByDealershipRequestResultLastW as $item) {
                if (!array_key_exists($item->name, $arrayForJs)) {
                    $arrayForJs[$item->name] = [];
                }
                if (!array_key_exists("Dn-7", $arrayForJs[$item->name])) {
                    $arrayForJs[$item->name]["Dn-7"] = 0;
                }
                if (!array_key_exists("Dn-7T", $arrayForJs[$item->name])) {
                    $arrayForJs[$item->name]["Dn-7T"] = 0;
                }
                $arrayForJs[$item->name]["Dn-7"] += $item->total;
                $arrayForJs[$item->name]["Dn-7T"] += $item->take;
                $countCallLastDay += $item->total;
                $countCallTakeLastDay += $item->take;
            }

            $finalArrayForJs = $arrayForJs;
            foreach ($arrayForJs as $key => $value) {
                if (!array_key_exists("Dn-0", $value)) {
                    $finalArrayForJs[$key]["Dn-0"] = 0;
                }
                if (!array_key_exists("Dn-0T", $value)) {
                    $finalArrayForJs[$key]["Dn-0T"] = 0;
                }
                if (!array_key_exists("Dn-7T", $value)) {
                    $finalArrayForJs[$key]["Dn-7T"] = 0;
                }
                if (!array_key_exists("Dn-7", $value)) {
                    $finalArrayForJs[$key]["Dn-7"] = 0;
                }
                if ($finalArrayForJs[$key]["Dn-0"] > 0) {
                    $finalArrayForJs[$key]["PDn-0"] = round( $finalArrayForJs[$key]["Dn-0T"] / $finalArrayForJs[$key]["Dn-0"] * 100);
                } else {
                    $finalArrayForJs[$key]["PDn-0"] = -1.0;
                }
                if ($finalArrayForJs[$key]["Dn-7"] > 0) {
                    $finalArrayForJs[$key]["PDn-7"] = round( $finalArrayForJs[$key]["Dn-7T"] / $finalArrayForJs[$key]["Dn-7"] * 100);
                } else {
                    $finalArrayForJs[$key]["PDn-7"] = -1.0;
                }
            }

            $totalCall = $countCallThisDay;
            $totalCallDiff = $countCallThisDay - $countCallLastDay;
            $percentDay = $countCallThisDay > 0 ? $countCallTakeThisDay / $countCallThisDay * 100 : 0; //care division by 0
            $percentLastDay = $countCallLastDay > 0 ? $countCallTakeLastDay / $countCallLastDay * 100 : 0; //care division by 0
            $percentDiffDay = $percentDay -$percentLastDay ;
            if(empty($finalArrayForJs))
            {
                $this->render('home_miss_data', [
                    'date' => isset($_SESSION["dealership_selected"]),
                    'dealership_selected' => isset($_SESSION["date_select"]),
                ]);
            }
            $this->render('home', [
                'totalCall' => $totalCall,
                'totalCallDiff' => $totalCallDiff,
                'percentDay' => round($percentDay),
                'percentDiffDay' => round($percentDiffDay),
                'arrayDealershipData' => $finalArrayForJs,
                'day' => $day
            ]);
        } else {
            $this->render('home_miss_data', [
                'date' => isset($_SESSION["date_select"]),
                'dealership_selected' => isset($_SESSION["dealership_selected"]) && !empty($_SESSION["dealership_selected"]),
            ]);
        }

    }


}