<?php

namespace app\Import\CSV;





class CSVConvertor {

    public static function ConvertFromFile($path,$params = []){

        $file = fopen($path,"r");
        $BasicArray = [];
        while(! feof($file))
        {
            $BasicArray[] = fgetcsv($file)[0]??[];
        }
        $headers = self::splitLine($BasicArray[0],$params);

        $returnArray = [];
        for($i = 1;$i < count($BasicArray)-1;$i++)
        {
            $splitedLine = self::splitLine($BasicArray[$i],$params);
            $arrayLine = [];
            for($j = 0;$j < count($splitedLine);$j++)
            {
                $arrayLine[$headers[$j%count($headers)]] = $splitedLine[$j];
            }
            $returnArray[] = $arrayLine;
        }

        fclose($file);
        return $returnArray;
    }

    private static function splitLine($line,$params = []){
        $separator = '';
        if(isset($params['separator'])){
            $separator = $params['separator'];
        }
        if([] === $params){
            $separator = ';';
        }

        return explode($separator,$line);
    }

}