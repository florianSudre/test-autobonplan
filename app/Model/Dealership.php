<?php


namespace app\Model;


use Core\Model\AbstractModel;
use Core\Model\Trait\UseStaticQuery;

class Dealership extends AbstractModel
{
    use UseStaticQuery;
    private $id;
    private $name;
    private $salepoint_phone;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name): void
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getSalepointPhone()
    {
        return $this->salepoint_phone;
    }

    /**
     * @param mixed $salepoint_phone
     */
    public function setSalepointPhone($salepoint_phone): void
    {
        $this->salepoint_phone = $salepoint_phone;
    }
}