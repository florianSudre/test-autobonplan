<?php


namespace app\Model;




use Core\Model\AbstractModel;

class DealershipCall extends AbstractModel {

    private $id;
    private $ext_id;
    private $duration;
    private $call_date;
    private $dealership_id;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getExtId()
    {
        return $this->ext_id;
    }

    /**
     * @param mixed $ext_id
     */
    public function setExtId($ext_id): void
    {
        $this->ext_id = $ext_id;
    }

    /**
     * @return mixed
     */
    public function getDuration()
    {
        return $this->duration;
    }

    /**
     * @param mixed $duration
     */
    public function setDuration($duration): void
    {
        $this->duration = $duration;
    }

    /**
     * @return mixed
     */
    public function getCallDate()
    {
        return $this->call_date;
    }

    /**
     * @param mixed $call_date
     */
    public function setCallDate($call_date): void
    {
        $this->call_date = $call_date;
    }

    /**
     * @return mixed
     */
    public function getDealershipId()
    {
        return $this->dealership_id;
    }

    /**
     * @param mixed $dealership_id
     */
    public function setDealershipId($dealership_id): void
    {
        $this->dealership_id = $dealership_id;
    }


}