<?php

use Core\Routing\RoutingConfigurator;

function Routes (RoutingConfigurator $routes){

     $routes->add('home')
         ->controller([\app\Controller\HomeController::class,'index'])
         ->methods(['GET'])
         ->create();

     $routes->add('register')
         ->controller([\app\Controller\Auth\Register::class,'index'])
         ->methods(['GET','POST'])
         ->create();

    $routes->add('login')
        ->controller([\app\Controller\Auth\Login::class,'index'])
        ->methods(['GET','POST'])
        ->create();
    $routes->add('logout')
        ->controller([\app\Controller\Auth\Logout::class,'index'])
        ->methods(['GET'])
        ->create();
    $routes->add('import')
        ->controller([\app\Controller\ImportController::class,'import'])
        ->methods(['POST'])
        ->create();

    $routes->add('setting')
        ->controller([\app\Controller\SettingController::class,'setting'])
        ->methods(['GET','POST'])
        ->create();
    return $routes->getRoutes();
};


 return Routes(new RoutingConfigurator());