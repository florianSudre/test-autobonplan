```
CREATE TABLE user
(
	id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    name VARCHAR(100) NOT NULL,
    email VARCHAR(255) UNIQUE,
    password VARCHAR(160) NOT NULL,
    permission JSON NOT NULL,
    created_at DATE NOT NULL
)

CREATE TABLE dealership
(
	id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    name VARCHAR(100) NOT NULL,
    salepoint_phone INT UNSIGNED UNIQUE NOT NULL
)

CREATE TABLE dealership_call
(
	id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
   ext_id VARCHAR(30) UNIQUE NOT NULL,
   duration INT UNSIGNED NULL,
   call_date DATETIME NOT NULL,
   dealership_id INT NOT NULL,
   FOREIGN KEY (dealership_id) REFERENCES dealership(id)
)
```