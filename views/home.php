<section class="page-content">
    <div class="page-content-header">
        <div class="page-content-title">
            <p>Bonjour ! Nous sommes <strong><?=$day?></strong></p>
            <p>Voici ce qui a changé depuis <?=$day?> dernier</p>
        </div>
        <div class="page-content-info">
            <div class="call-recive">
                <div>
                    <p>total appel reçus</p>
                    <div class="call-recive-numbers">
                        <span><?= $totalCall ?></span>
                        <?php
                        if ($totalCallDiff > 0) {
                            echo '<span class="indicator-positive">' . $totalCallDiff . '</span>';
                        } else {
                            echo '<span class="indicator-negative">' . $totalCallDiff . '</span>';
                        }
                        ?>
                    </div>
                </div>
            </div>
            <div class="take-in-charge">
                <div>
                    <p>total prise en charge</p>
                    <div class="take-in-charge-numbers">
                        <span><?= $percentDay ?>%</span>
                        <?php
                        if ($percentDiffDay > 0) {
                            echo '<span class="indicator-positive">' . $percentDiffDay . '%</span>';
                        } else {
                            echo '<span class="indicator-negative">' . $percentDiffDay . '%</span>';
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>
        <!-- The data encoding type, enctype, MUST be specified as below -->
        <?php
        if (isset($sessionUser) && $permissionImport) {
            echo '<div class="page-content-upload-btn">
            <form id="upload-form" enctype="multipart/form-data" hidden action="http://local.test-auto.com/import"
                  method="POST">
                <input id="input-file-csv" name="userfile" type="file"/>
                <button type="submit">Submit</button>
            </form>
            <button>    <span>Uploader</span>
                <svg width="24px" height="24px" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">

                    <title/>

                    <g id="Complete">

                        <g id="F-File">

                            <g id="Add">

                                <path d="M18,22H6a2,2,0,0,1-2-2V4A2,2,0,0,1,6,2h7.1a2,2,0,0,1,1.5.6l4.9,5.2A2,2,0,0,1,20,9.2V20A2,2,0,0,1,18,22Z"
                                      fill="none" id="File" stroke="#000000" stroke-linecap="round"
                                      stroke-linejoin="round" stroke-width="2"/>

                                <line fill="none" stroke="#000000" stroke-linecap="round" stroke-linejoin="round"
                                      stroke-width="2" x1="12" x2="12" y1="17" y2="11"/>

                                <line fill="none" stroke="#000000" stroke-linecap="round" stroke-linejoin="round"
                                      stroke-width="2" x1="9" x2="15" y1="14" y2="14"/>
                            </g>

                        </g>

                    </g>

                </svg>
            </button>
        </div>';
        }
        ?>
    </div>
    <div class="page-content-grid">
        <p>Appels reçus par concession</p>
        <!--            <div id="chartContainer" style="height: 370px; width: 100%;"></div>-->
        <canvas id="myChart"></canvas>
    </div>
    <div class="page-content-stats">
        <p class="title-page-content-stats">Taux de prise en charge par concession</p>

        <?php
        foreach ($arrayDealershipData as $key => $value) {

            $percentThisDay = ($value["PDn-0"]> 0 ?$value["PDn-0"].'%':null);
            $percentLastDay = ($value["PDn-7"]> 0 ?$value["PDn-7"].'%':null);


            echo '<div class="line-stats">
            <div class="title-line-stats">
                <span> '.$key.'</span>
            </div>
            <div class="first-line-stats">
                <span style="width: '.($percentThisDay??0).'"></span>
                <span>'.($percentThisDay??"pas d'appel pris en charge").'</span>
            </div>
            <span class="second-line-stats">
                    <span style="width: '.($percentLastDay??0).'"></span>
                <span>'.($percentLastDay??"pas d'appel pris en charge").'</span>
                </span>
        </div>';
        }?>
    </div>
</section>

<script type="text/javascript">var jArray =<?php echo json_encode($arrayDealershipData); ?>;</script>
<!-- Custom JS -->
