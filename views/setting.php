<section class="setting-content">
    <p>Sélectionner des parametre puis cliquer sur <strong>Appliquer</strong></p>
    <button class="setting-reset" > Tout désélectionné</button>
    <form class="setting-form" name="form-content" action="http://local.test-auto.com/setting" method="post">

        <label><input type="date" name="date" value="<?= $date ?>"> select date</label>
        <div class="dealership-selection">

            <div class="detail-dealership-selection">
                <p>Selection au Detail</p>
                <?php
                foreach ($dealerships as $dealership) {
                    echo '
                    <label>
                    <input type="checkbox" 
                    id="' . $dealership->id . '"
                     name="checked-' . $dealership->id . '"' . ' ' . ($dealership->checked ? "checked" : "") . '
                     
                      >
                    ' . $dealership->name . ' : ' . $dealership->salepoint_phone . '
                    </label>';
                }
                ?>
            </div>
            <div class="fast-dealership-selection">
                <p>Selection rapide</p>
                <?php
                foreach ($dealershipsFastSelection as $key => $value) {
                    echo '
                    <label>
                    <input type="checkbox" 
                    class="fast-selection"
                    id="[' . implode(',',$value) . ']">
                    ' . $key . '
                    </label>';
                }
                ?>
            </div>
        </div>

        <button class="flying-down-button" type="submit"> Appliquer</button>
    </form>


</section>

<!-- Custom JS -->
