
<div class="register">
    <div class="register-form">

    <form action="/register" method="POST">

        <label for="name">Name</label>
        <input type="text" id="name" name="name">

        <label for="email">Email</label>
        <input type="text" id="email" name="email">

        <label for="password">Password</label>
        <input type="password" id="password" name="password">

        <button class="login-register-button" type="submit">Register</button>
    </form>
        <div class="other-choice">
        <p>OU</p>
        <a class="login-register-button" href="/login">Login</a>
        </div>
    </div>
</div>