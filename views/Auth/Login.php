
<div class="login">
    <div class="login-form">

    <form action="/login" method="POST">

        <label for="email">Email</label>
        <input type="text" id="email" name="email">

        <label for="password">Password</label>
        <input type="password" id="password" name="password">

        <button class="login-register-button" type="submit">Login</button>

    </form>
        <div class="other-choice">
            <p>OU</p>
            <a class="login-register-button" href="/register">Register</a>
        </div>
    </div>

</div>