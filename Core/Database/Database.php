<?php


namespace Core\Database;

use PDO;
use ReflectionClass;
use Core\Tools\Converter\StringConverter as SC;
class Database
{
    private static $instance;
    private $db_name;
    private $db_user;
    private $db_pass;
    private $db_host;

    private $pdo;

    public static function getInstance(){
        if(self::$instance === null)
        {
            self::$instance = new self(
                getenv('DATABASE_NAME'),
                getenv('DATABASE_USER'),
                getenv('DATABASE_PASSWORD'),
                getenv('DATABASE_HOST')
            );
        }
        return self::$instance;
    }
    public function __construct($db_name, $db_user, $db_pass, $db_host)
    {
        $this->db_name = $db_name;
        $this->db_user = $db_user;
        $this->db_pass = $db_pass;
        $this->db_host = $db_host;
    }

    private function getPDO()
    {
        if ($this->pdo === null) {
            $dns = 'mysql:dbname=' . $this->db_name . ';host=' . $this->db_host;
            $pdo = new PDO($dns, $this->db_user, $this->db_pass);
            $pdo->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
            $this->pdo = $pdo;
        }
        return $this->pdo;
    }

    public function query($statement,$attribute = [],$class_name = null){
        $req = $this->prepare($statement,$attribute);
        if($class_name === null){
            $data = $req->fetchAll(PDO::FETCH_OBJ) ;
        }else{
            $data = $req->fetchAll(PDO::FETCH_CLASS,$class_name) ;
        }

        return $data;
    }

    public function prepare($statement,$attributes){
//        echo '<pre>';
//        var_dump($statement);
//        var_dump($attributes);
//        echo '</pre>';
        $req = $this->getPDO()->prepare($statement,[PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY]);
        $req->execute($attributes);
        return $req;
    }


    public function insertMany($objects,$className){

        $class = new ReflectionClass($className);
        $properties = $class->getProperties();
        $className = SC::converCameltoSnake(SC::getClassName($className));//ClassName to class_name

        $propertiesKeys = ' (';
        $is_first = true;
        $propertiesArray = [];

        foreach ($properties as $property){
            $propertyName = $property->getName();
            if($propertyName !== 'id'){
                $CamelName = SC::convertSnakeToPascal($propertyName);//property_name to PropertyName
                $propertyGetter = 'get'.$CamelName;

                $propertiesArray[] = $propertyGetter;
                $propertiesKeys .= $is_first?'':',';
                $propertiesKeys .= $propertyName;

                $is_first = false;
            }
        }
        $propertiesKeys .=')';
        $propertiesValueArray = [];
        foreach ($objects as $object){
            $is_first = true;
            $propertiesValues = ' (';
            foreach ($propertiesArray as $property){

                $propertyValue = $object->$property();// access to $object->getPropertyName()
                $propertiesValues .= $is_first?'':',';

                if(empty($propertyValue))
                {
                    $propertiesValues .= 'NULL';
                }else{
                    $propertiesValues .= is_string($propertyValue) ? '"'.$propertyValue.'"': $propertyValue;
                }

                $is_first = false;

            }
            $propertiesValues .=')';
            $propertiesValueArray[] = $propertiesValues;
        }


        $objectsValues =  implode(',', $propertiesValueArray);


        return  $this->query('INSERT INTO '.$className.$propertiesKeys.' VALUES'. $objectsValues);
    }

}