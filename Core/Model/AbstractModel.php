<?php

namespace Core\Model;

use Core\Database\Database;
use Core\Tools\Converter\StringConverter as SC;
use ReflectionClass;

abstract class AbstractModel{

    public function __get($key){
        $methode = 'get'. ucfirst($key);
        $this->$key = $this->$methode();
        return $this->$key;
    }


    public function query($statement,$attribute = []){
       return Database::getInstance()->query($statement,$attribute,get_class($this));
    }

    public function getAll(){
        $className = SC::getClassNameFromObject($this);
        return Database::getInstance()->query('SELECT * FROM '.strtolower($className),[],get_class($this));
    }

    public function getOneById($id){
        $className = SC::getClassNameFromObject($this);
        return Database::getInstance()->query('SELECT * FROM '.strtolower($className).' WHERE id ='.$id,[],get_class($this))[0];
    }

    public function getOneBy($attributes){
        $className = SC::getClassNameFromObject($this);
        $conditions = "";
        $sqlCondition = 'WHERE';
        foreach ($attributes as $key => $value){
            $conditions = ' '.$sqlCondition.' '.$key.' = :'.$key;
            if($sqlCondition === 'WHERE'){
                $sqlCondition = 'AND';
            }
        }
        return Database::getInstance()->query('SELECT * FROM '.strtolower($className).$conditions,$attributes,get_class($this));
    }


    public function insert(){
        $class = new ReflectionClass(get_class($this));
        $properties = $class->getProperties();
        $className = SC::converCameltoSnake(SC::getClassNameFromObject($this));//ClassName to class_name
        
        
        $propertiesKeys = ' (';
        $propertiesValues = ' (';
        $is_first = true;
        foreach ($properties as $property){
            $propertyName = $property->getName();
            if($propertyName !== 'id'){
                $CamelName = SC::convertSnakeToPascal($propertyName);//property_name to PropertyName
                $propertyGetter = 'get'.$CamelName;
                $propertyValue = $this->$propertyGetter();// access to getPropertyName()

                $propertiesKeys .= $is_first?'':',';
                $propertiesValues .= $is_first?'':',';

                $propertiesKeys .= $propertyName;
                $propertiesValues .= is_string($propertyValue) ? '"'.$propertyValue.'"': $propertyValue;

                $is_first = false;
            }
        }
        $propertiesKeys .=')';
        $propertiesValues .=')';
        return $this->query('INSERT INTO '.$className.$propertiesKeys.' VALUES'. $propertiesValues);
    }


}



