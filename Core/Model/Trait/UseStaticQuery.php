<?php


namespace Core\Model\Trait;




use Core\Database\Database;

trait UseStaticQuery{
    public static function queryS($statement,$attribute = []){
        return Database::getInstance()->query($statement,$attribute,self::class);
    }

}