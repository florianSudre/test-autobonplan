<?php

use Core\Autoloader;

if (true === (require_once __DIR__.'/autoload.php')) {
    return;
}

Autoloader::register();
