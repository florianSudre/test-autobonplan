<?php

namespace Core\Controller;


abstract class AbstractController
{

    protected function render($vue, $parameters = [])
    {
        $rootPath = $_SERVER['DOCUMENT_ROOT'];
        $viewPath = $rootPath . DIRECTORY_SEPARATOR . 'views' . DIRECTORY_SEPARATOR . str_replace('.', DIRECTORY_SEPARATOR, $vue) . '.php';
        $template = $rootPath . DIRECTORY_SEPARATOR . 'views' . DIRECTORY_SEPARATOR . 'template'.DIRECTORY_SEPARATOR. 'base_layout.php';
        ob_start();

        extract($parameters);

        require $viewPath;

        $content = ob_get_clean();

        require $template;
    }

    protected function redirect($vue, $parameters = [])
    {


    }

}