<?php

namespace Core;
final class Autoloader
{

    public static function register()
    {
        spl_autoload_register(array(__CLASS__, 'autoload'));
    }

    static function autoload($class_name)
    {
        $class_name = str_replace('\\', DIRECTORY_SEPARATOR, $class_name);
        require $_SERVER["DOCUMENT_ROOT"] . DIRECTORY_SEPARATOR . $class_name . '.php';
    }

}
