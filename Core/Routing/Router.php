<?php

namespace Core\Routing;


final class Router
{
     const METHOD_GET = 'GET';
     const METHOD_POST = 'POST';
    private $url;

    private $routes = [];

    public function __construct($url)
    {
        $this->url = $url;
        $rootApp = str_replace('Core','',dirname(__DIR__));
        $this->loadRoutes(require $rootApp.'app'.DIRECTORY_SEPARATOR.'Config'.DIRECTORY_SEPARATOR.'route.php');
    }

    public function loadRoutes($routes)
    {

        $this->routes = $this->organizeRoutes($routes);
    }

    public function organizeRoutes($routes)
    {
        $organizedRoutes = [];
        foreach ($routes as $route) {
            if($route->getMethods() === null)
            {
                $route->setMethods([self::METHOD_GET,self::METHOD_POST]);
            }
            foreach ($route->getMethods() as $methode){
                $organizedRoutes[$methode][] = $route;
            }
        }

        return $organizedRoutes;
    }

    public function run(){
//        echo '<pre>';
//        echo print_r($this->routes);
//        echo '</pre>';

        $method = $_SERVER['REQUEST_METHOD'];
        if(!isset($this->routes[$method])) {
            throw new \Exception('No route method matches for ');
        }

        foreach ($this->routes[$method] as $route) {
            if($route->match($this->url)){
              

                return $route->call();
            }
        }
        throw new \Exception('No route matches');
    }
}