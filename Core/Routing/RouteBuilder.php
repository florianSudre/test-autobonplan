<?php

namespace Core\Routing;

final class RouteBuilder
{
    private $routeConfigurator;
    private $route;
    public function __construct($path,$routeConfigurator){
        $this->routeConfigurator = $routeConfigurator;
        $this->add($path);
        return $this;
    }

    private function Add($path){
        $this->route = new Route();
        $this->route->setPath($path);
    }

    public function controller($callable){
        $this->route->setCallable($callable);
        return $this;
    }

    public function methods($array){
        $this->route->setMethods($array);
        return $this;
    }

    public function create()
    {
        $this->routeConfigurator->addRoute($this->route);
    }

}