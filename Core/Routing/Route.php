<?php

namespace Core\Routing;


final  class Route
{

    private $path;
    private $callable;
    private $method;


    public function match($url)
    {
        $url = trim($url, '/');
        $path = preg_replace('#:([\w]+)#', '([^/]+)', $this->path);
        $regex = "#^$path$#i";
        if(!preg_match($regex,$url,$matches))
        {
            return false;
        }
        return true;
    }

    public function call()
    {
        if (is_array($this->callable)) {
            $controller = $this->callable[0];
            $action = $this->callable[1];

            $controller = new $controller();
            return $controller->$action();
        }
    }


    public function setPath($path)
    {
        $this->path = $path;
    }

    public function getPath()
    {
        return $this->path;
    }

    public function setCallable($callable)
    {
        $this->callable = $callable;
    }

    public function getCallable()
    {
        return $this->callable;
    }

    public function setMethods($method)
    {
        $this->method = $method;
    }

    public function getMethods()
    {
        return $this->method;
    }
}
