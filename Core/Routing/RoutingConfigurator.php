<?php


namespace Core\Routing;



final class RoutingConfigurator{

    private $routes;

    public function add($url){
        return New RouteBuilder($url,$this);
    }

    public function addRoute($route)
    {
        $this->routes[] = $route;
    }

    public function getRoutes()
    {
       return $this->routes;
    }

}