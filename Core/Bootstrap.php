<?php


namespace Core;

use Core\Routing\Router;

class Bootstrap {


    private $Router ;
    private $container;
    public function __construct($request)
    {
        (new DotEnv($_SERVER['DOCUMENT_ROOT'] . '/.env'))->load();


        switch ($_ENV["APP_ENV"])
        {
            case 'dev': $request = $request;break;
            case 'prod':
                $request["url"] = array_keys($request)[0];
                ;break;
        }
        if(strpos($request['url'], 'assets') !== false){
            header('content-type: '. explode(',',$_SERVER['HTTP_ACCEPT'])[0]);
            include str_replace('Core','',__DIR__).$request['url'];
        }
        else
        {
            $this->Router = new Router($request['url']);
            $this->Router->run();
        }



    }

}