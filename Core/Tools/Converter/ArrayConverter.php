<?php


namespace Core\Tools\Converter;




class ArrayConverter
{
    public static function arrayToStringSQL($array){

        $i = 0;
        $in ='';
        $in_params =[];
        foreach ($array as $item)
        {
            $key = ":id".$i++;
            $in .= ($in ? "," : "") . $key; // :id0,:id1,:id2
            $in_params[$key] = $item; // collecting values into a key-value array
        }

        $result =array_merge($array,$in_params);
        $returnArray = ["param"=>$in_params,"in"=>$in];

        return $returnArray;
    }

}