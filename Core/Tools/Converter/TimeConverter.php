<?php


namespace Core\Tools\Converter;





class TimeConverter
{
    public static function StringToSeconds($secTime){
        if(!preg_match("/(?:[01]\d|2[0-3]):(?:[0-5]\d):(?:[0-5]\d)/",$secTime))
        {
            return false;
        }
        $timeArray = explode(':',$secTime);
        $seconds = 0;

        $seconds += intval($timeArray[0])*3600;
        $seconds += intval($timeArray[1])*60;
        $seconds += intval($timeArray[2]);
        return $seconds;
    }
    public static function secondsToString($secTime){

        if(!is_int($secTime))//check if int
        {
            return '0';
        }
        if($secTime > 86400)//check supp to 24h
        {
            return '0';
        }


        $hoursRest = $secTime%3600;
        $hours = ($secTime-$hoursRest) /3600;
        $secTime = $hoursRest;

        $minutesRest = $secTime%60;
        $minutes = ($secTime-$minutesRest) /60;
        $secTime = $minutesRest;

        $seconds = $secTime;

        $hoursString = '';
        $minutesString = '';
        $secondsString = '';

        if($hours <= 10)
        {
            $hoursString .= '0';
        }
        $hoursString .= strval($hours);


        if($minutes <= 10)
        {
            $minutesString .= '0';
        }
        $minutesString .= strval($minutes);

        if($seconds <= 10)
        {
            $secondsString .= '0';
        }
        $secondsString .= strval($seconds);

        return $hoursString.':'.$minutesString.':'.$secondsString;
    }

}