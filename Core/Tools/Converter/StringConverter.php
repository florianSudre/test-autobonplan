<?php


namespace Core\Tools\Converter;




class StringConverter
{
    public static function getClassNameFromObject($object){
       return self::getClassName(get_class($object));
    }
    public static function getClassName($class){
        $className = explode('\\',$class);
        $className = $className[count($className)-1];
        return $className;
    }
    public static function convertSnakeToPascal($key){
        return str_replace('_', '', ucwords($key, '_'));
    }
    public static function converCameltoSnake($key){
        return ltrim(strtolower(preg_replace('/[A-Z]([A-Z](?![a-z]))*/', '_$0', $key)), '_');
    }
}