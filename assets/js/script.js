
$( document ).ready(function() {
    $('.nav-toggler').click(function(){
        $('.nav ul:first-child').toggleClass('open')
        $('.page-content').toggleClass("min-nav")
    })
    
    $('.name').click(function(){
        $('.user-dropdown').toggle()
        $('.name i').toggleClass("fa-chevron-down")
        $('.name i').toggleClass("fa-chevron-up")
    })

})


$('.page-content-upload-btn button').click(function(){
    console.log('je click');
    $('#input-file-csv').click();
})
$('#input-file-csv').change(function(e){

        let formData = new FormData();

        let file = $('#input-file-csv')[0].files[0];
        formData.append('file',file);
        $.ajax({
            // Your server script to process the upload
            url: 'http://local.test-auto.com/import',
            type: 'post',
            data: formData,
            contentType: false,
            processData: false,
            success:function (data){
                if(data !== 0)
                {
                    alert(data);
                }else
                {
                    alert(data);
                }
                const file = document.querySelector('#input-file-csv');
                file.value = '';
            }
        }).fail(console.log)

});
$(".setting-reset").click(function() {
    $("input[type=checkbox]").prop("checked", false);
});
$('.fast-selection').each(function(){

    $(this).change(function (){
        el = $(this);
        JSON.parse($(this)[0].id).forEach(function (id){
            $('#'+id).prop('checked', el[0].checked);
        })

    });
})



if(jArray !== null){
    var keys = [];
    var Day = [];
    var LastDay = [];
    $.each(jArray, function(key, value) { keys.push(key);Day.push(value["Dn-0"]);LastDay.push(value["Dn-7"]); })

    var data = {
        labels: keys,
        datasets: [{
            backgroundColor: 'rgb(0,145,182)',
            borderWidth: 1,
            data: Day,
        }, {
            backgroundColor: 'rgb(251,185,125)',
            borderWidth: 1,
            data: LastDay,

        }]
    };



    var options = {
        plugins: {
            legend: {
                display: false
            }
        },
        scales: {

            xAxes: [{
                stacked: true,
                id: "bar-x-axis1",
                categoryPercentage: 0.2,
                type: 'category',
                barPercentage: 1,
            }, {
                display: false,
                stacked: true,
                id: "bar-x-axis2",
                type: 'category',
                categoryPercentage: 0.1,
                barPercentage: 0.1,
                gridLines: {
                    offsetGridLines: true
                },
                offset: true
            }],
            yAxes: [{
                stacked: false,
                ticks: {
                    beginAtZero: true
                },
            }]

        }
    };

    var ctx = document.getElementById("myChart").getContext("2d");
    var myBarChart = new Chart(ctx, {
        type: 'bar',
        data: data,
        options: options
    });
}





